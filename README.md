MCPat
=====

MCPat is Meta-Circular Pattern Matcher. It is a simple pattern matcher that has its compiler implemented in its own rule syntax.

# Quick Start

Check out code from git repository. Let's say you put it into `$HOME/common-lisp/mcpat`. Then, load it up with:

```
(asdf:load-system :mcpat)
```

Run basic tests with:

```
(load "~/common-lisp/mcpat/test.lisp")
```

# License

Software is released under [BSD](LICENSE).
 
# Acknowledgements

Continued development of this software is being supported by my employer, [ViaSat Inc](https://www.viasat.com/).
